﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="PageWeb.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div style="font-size: xx-large; background-color: #3366FF; text-align: center">
        User Registration Form
    </div>
    <br />
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label5" runat="server" Text="User ID:"></asp:Label>
            <asp:TextBox ID="idTxt" runat="server" Width="266px"></asp:TextBox>
        <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Update" />
        <asp:Button ID="Button4" runat="server" OnClick="Button4_Click" OnClientClick="return confirm('Are you sure to delete ?');" Text="Delete" />
            <br />
            <br />
            <br />
            <hr />
        </div>
        <div>
            <asp:Label ID="Label1" runat="server" Text="Name:"></asp:Label>
            <asp:TextBox ID="nameTxt" runat="server" style="margin-bottom: 0px" Width="281px"></asp:TextBox>
        </div>
        <p>
            <asp:Label ID="Label2" runat="server" Text="Year:"></asp:Label>
            <asp:TextBox type="number" ID="yearTxt" runat="server" Width="288px"></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="Label3" runat="server" Text="Date:"></asp:Label>
            <asp:TextBox type="date" ID="dateTxt" runat="server" Width="288px"></asp:TextBox>
        </p>
        <asp:Label ID="Label4" runat="server" Text="Gender:"></asp:Label>
        <asp:DropDownList ID="DropDownList1" runat="server">
            <asp:ListItem Value="man">Man</asp:ListItem>
            <asp:ListItem Value="woman">Woman</asp:ListItem>
        </asp:DropDownList>
        <br />
        <br />
        <asp:Button ID="Button1" runat="server" Text="Insert" OnClick="Button1_Click" />
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Search" />
        <p>
        <asp:Label ID="resultTxt" runat="server" Text="" style="color: #800000; font-size: large"></asp:Label>
        </p>
        <asp:GridView ID="GridView1" runat="server" Width="542px">
        </asp:GridView>
    </form>
</body>
</html>
