﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PageWeb.ServiceReference1;

namespace PageWeb
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        ServiceReference1.Service1Client client = new ServiceReference1.Service1Client();
        protected void Button1_Click(object sender, EventArgs e)
        {
            InsertUser u = new InsertUser();
            u.Name = nameTxt.Text;
            u.Year = Convert.ToInt32(yearTxt.Text);
            u.Day = dateTxt.Text;
            u.Gender = DropDownList1.SelectedValue;
            string result = client.Insert(u);
            resultTxt.Text = result;
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            ServiceReference1.GettestData gt = new ServiceReference1.GettestData();
            gt = client.GetInfo();
            DataTable dt = new DataTable();
            dt = gt.userTable;
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            UpdateUser u = new UpdateUser();
            u.Id = Convert.ToInt32(idTxt.Text);
            u.Name = nameTxt.Text;
            u.Year = Convert.ToInt32(yearTxt.Text);
            u.Day = dateTxt.Text;
            u.Gender = DropDownList1.SelectedValue;
            string result = client.Update(u);
            resultTxt.Text = result;
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            DeleteUser d = new DeleteUser();
            d.Id = Convert.ToInt32(idTxt.Text);
            string result = client.Delete(d);
            resultTxt.Text = result;
        }
    }
}