﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;

namespace PageWeb_CRUD_with_WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {

        [OperationContract]
        string Insert(InsertUser user);

        [OperationContract]
        GettestData GetInfo();

        [OperationContract]
        string Update(UpdateUser u);

        [OperationContract]
        string Delete(DeleteUser d);
        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class GettestData
    {
        [DataMember]
        public DataTable userTable
        {
            get;
            set;
        }
    }

    [DataContract]
    public class InsertUser
    {
        string name = string.Empty;
        int year;
        string day = string.Empty;
        string gender = string.Empty;
        //bool boolValue = true;
        //string stringValue = "Hello ";

        [DataMember]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [DataMember]
        public int Year
        {
            get { return year; }
            set { year = value; }
        }

        [DataMember]
        public string Day
        {
            get { return day; }
            set { day = value; }
        }

        [DataMember]
        public string Gender
        {
            get { return gender; }
            set { gender = value; }
        }
    }

    [DataContract]
    public class UpdateUser
    {
        int id;
        string name;
        int year;
        string day;
        string gender;

        [DataMember]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        [DataMember]
        public int Year
        {
            get { return year; }
            set { year = value; }
        }

        [DataMember]
        public string Day
        {
            get { return day; }
            set { day = value; }
        }

        [DataMember]
        public string Gender
        {
            get { return gender; }
            set { gender = value; }
        }
    }

    [DataContract]
    public class DeleteUser
    {
        int id;

        [DataMember]
        public int Id
        {
            get { return id; }
            set { id = value; }
        }
    }
}
