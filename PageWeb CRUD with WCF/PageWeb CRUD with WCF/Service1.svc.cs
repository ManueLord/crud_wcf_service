﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data;
using MySql.Data.MySqlClient;

namespace PageWeb_CRUD_with_WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        string connection = "datasource=localhost;port=3306;user=root;password=;database=simple_crud_db;";
        string sql;

        public string Insert(InsertUser user)
        {
            sql = "INSERT INTO listpeople(name, year, date, gender) VALUES (@Name, @Year, @Day, @Gender)";
            MySqlConnection con = new MySqlConnection(connection);
            try
            {
                con.Open();
                MySqlCommand cmd = new MySqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@Name", user.Name);
                cmd.Parameters.AddWithValue("@Year", user.Year);
                cmd.Parameters.AddWithValue("@Day", user.Day);
                cmd.Parameters.AddWithValue("@Gender", user.Gender);
                cmd.ExecuteNonQuery();
                con.Close();
                return "Successfully Inserted";
            }
            catch(Exception e)
            {
                return e.Message;
            }
        }

        
        public GettestData GetInfo()
        {
            GettestData gd = new GettestData();
            sql = "SELECT * FROM listpeople";
            MySqlConnection con = new MySqlConnection(connection);
            con.Open();
            MySqlCommand cmd = new MySqlCommand(sql, con);
            MySqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            gd.userTable = dt;
            dr.Close();
            con.Close();
            return gd;
        }

        public string Update(UpdateUser u)
        {
            sql = "UPDATE listpeople SET name = @Name, year = @Year, date = @Day, gender = @Gender WHERE id = @Id";
            MySqlConnection con = new MySqlConnection(connection);
            try
            {
                con.Open();
                MySqlCommand cmd = new MySqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@Id", u.Id);
                cmd.Parameters.AddWithValue("@Name", u.Name);
                cmd.Parameters.AddWithValue("@Year", u.Year);
                cmd.Parameters.AddWithValue("@Day", u.Day);
                cmd.Parameters.AddWithValue("@Gender", u.Gender);
                cmd.ExecuteNonQuery();
                con.Close();
                return "Successfully Update";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string Delete(DeleteUser d)
        {
            sql = "DELETE FROM listpeople  WHERE id = @Id";
            MySqlConnection con = new MySqlConnection(connection);
            try
            {
                con.Open();
                MySqlCommand cmd = new MySqlCommand(sql, con);
                cmd.Parameters.AddWithValue("@Id", d.Id);
                cmd.ExecuteNonQuery();
                con.Close();
                return "Successfully Delete";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}
